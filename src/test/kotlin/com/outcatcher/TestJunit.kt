package com.outcatcher

import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.Selenide
import com.codeborne.selenide.Selenide.open
import com.outcatcher.pages.ExecutionPage
import com.outcatcher.pages.LoginPage
import com.outcatcher.widgets.MessageType.SUCCESS
import io.qameta.allure.Description
import io.qameta.allure.Step
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

private fun validLogin() {
    open("/login")
    LoginPage().signIn("akachuri", SECRET_PASSWORD)
}

/**
 * Tests for `/login` page
 */
class LoginTests : BaseUITest() {

    private val loginPage = LoginPage()
    private val executionPage = ExecutionPage()

    @Step
    @Description("Open Browser To Login Page")
    @BeforeEach
    fun testSetup() {
        loginPage.open()
    }

    @AfterEach
    fun cleanup() {
        Selenide.clearBrowserCookies()
        Selenide.clearBrowserLocalStorage()
    }

    @Test
    fun `positive login`() {
        loginPage.signIn("akachuri", SECRET_PASSWORD)
        executionPage.shouldBeOpen()
    }

    @Test
    fun `negative login`() {
        loginPage.signIn("asdsde", "jiojdgfsk")
        loginPage.shouldBeOpen()
    }

}

/**
 * Tests for `/execution` page
 */
class ExecutionTests : BaseUITest() {

    private val executionPage = ExecutionPage()

    @Step
    @BeforeEach
    fun `open executions page`() {
        validLogin()
        open("/execution")
    }

    @Test
    fun `dry run`() {
        executionPage.startExecution(
            "ELD Test", "Smoke",
            email = "anton.kachurin@t-systems.com", dryRun = true,
            suiteIDs = listOf("Tests.01 Driver.Driver Create")
        )
        with(executionPage.result) {
            assure(visible)
            messageType shouldBe SUCCESS
            text shouldBe "Data is valid"
        }
    }
}
