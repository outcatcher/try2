package com.outcatcher

import com.codeborne.selenide.Configuration
import com.codeborne.selenide.junit5.BrowserStrategyExtension
import io.github.bonigarcia.wdm.WebDriverManager
import io.qameta.allure.Allure.step
import io.qameta.allure.model.Status
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.extension.ExtendWith

const val SECRET_PASSWORD = "search configrt"

@ExtendWith(BrowserStrategyExtension::class)
abstract class BaseUITest {


    companion object {
        @JvmStatic
        @BeforeAll
        fun suiteSetup() {
            WebDriverManager.chromedriver().version("76.0").setup()
            Configuration.headless = true
            Configuration.baseUrl = "https://manhwa.caritc.com"
        }

    }
}

infix fun <A, B : A> A.shouldBe(b: B) {
    val status = if (this != b) Status.FAILED else Status.PASSED
    step("`$this` should be `$b`", status)
    if (status == Status.FAILED) {
        throw AssertionError("Expected to get $b, got $this")
    }
}
