import com.codeborne.selenide.Configuration
import com.outcatcher.pages.ExecutionPage
import com.outcatcher.pages.LoginPage
import spock.lang.Specification

abstract class SmokeTest extends Specification {
    def loginPage = new LoginPage()
    def executionPage = ExecutionPage.INSTANCE

    def setupSpec() {
        Configuration.baseUrl = "https://manhwa.caritc.com"
    }

    def setup() {
        loginPage.open()
        loginPage.signIn("akachuri", "no, it's a secret")
    }
}


class ExecutionTest extends SmokeTest {

//    def "dry run"() {
//        when:
//        executionPage.startExecution(
//                "ELD Test", "Smoke",
//                "anton.kachurin@t-systems.com", true,
//                ["Tests.01 Driver.Driver Create", "Tests.03 Vehicle"]
//        )
//
//        then:
//        executionPage.result.assure(visible)
//        executionPage.result.messageType == MessageType.SUCCESS
//        executionPage.result.text == "Data is valid"
//    }
}
