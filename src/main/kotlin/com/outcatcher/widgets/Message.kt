package com.outcatcher.widgets

enum class MessageType { SUCCESS, FAIL }

class Message(cssSelector: String) : BaseWidget(cssSelector) {
    val messageType: MessageType
        get() {
            val cls = getAttribute("class")
            return if ("alert-success" in cls) MessageType.SUCCESS else MessageType.FAIL
        }
}