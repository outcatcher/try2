package com.outcatcher.widgets

import com.outcatcher.elements.TextInput
import org.openqa.selenium.By.ById


class LoginWidget : BaseWidget("form.login-form") {
    val username = TextInput(ById("username"))
    val password = TextInput(ById("password"))
    val signIn = find(ById("sign-in"))
}