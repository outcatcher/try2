package com.outcatcher.widgets

import com.codeborne.selenide.Condition
import com.codeborne.selenide.Condition.hidden
import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.Driver
import com.codeborne.selenide.Selectors.byId
import com.codeborne.selenide.SelenideElement
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select as WSelect


/**
 * Bootstrap select widget
 */
class Select(labelLocator: String) : BaseWidget(getSelectRootByLabel(labelLocator)) {
    private val triggerButton = find("button.dropdown-toggle")
    override fun getText(): String = triggerButton.find("span").text

    private val dropdownList = find("div.dropdown-menu")
    private val optionsListCss = "li > a"
    private val optionsList = dropdownList.findAll(optionsListCss)

    private val shadowedSelect
        get() = WSelect(find(byId(triggerButton.getAttribute("data-id"))))

    /**
     * Check that number of dropdown options is same as in shadowed select
     */
    private val constructedCondition = object : Condition("constructed") {
        override fun apply(driver: Driver, element: WebElement): Boolean {
            val options = element.findElements(By.cssSelector(optionsListCss))
                .filter { it.getAttribute("value") != "" }
            val shadowedOptions = shadowedSelect.options
                .filter { it.getAttribute("value") != "" }
            return options.size == shadowedOptions.size
        }
    }

    override fun selectOption(vararg text: String) {
        assure(constructedCondition)
        triggerButton.click()
        dropdownList.shouldBe(visible)
        val loweredValues = text.map { it.toLowerCase() }
        optionsList
            .filter { it.text.toLowerCase() in loweredValues }
            .forEach { it.click() }
        if (dropdownList.isDisplayed) {
            triggerButton.click()
            dropdownList.shouldBe(hidden)
        }
    }

    companion object {
        private fun getSelectRootByLabel(labelLocator: String): SelenideElement {
            val label = rootFind(labelLocator)!!
            return label.parent().find("div.bootstrap-select")
        }
    }

}
