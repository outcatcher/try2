package com.outcatcher.widgets

import com.outcatcher.elements.TextInput

object StartExecutionWidget : BaseWidget("#execution-start-form") {
    @JvmStatic
    val environment = Select("label[for='execution-environment']")
    @JvmStatic
    val typeOfTest = Select("label[for='test-type']")
    @JvmStatic
    val email = TextInput("#email-input")
    @JvmStatic
    val suites = TreeView("#suites-tree")
    @JvmStatic
    val dryRun = find("#dry-run")
    @JvmStatic
    val startButton = find("#execution-start-btn")
}
