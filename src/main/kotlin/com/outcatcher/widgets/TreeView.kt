package com.outcatcher.widgets

import com.codeborne.selenide.SelenideElement
import io.qameta.allure.Step

private fun dataId(node: SelenideElement): String = node.getAttribute("data-id")

/**
 * Item containing expander (+/-), checkbox (optional) and text. Node must be 'li.list-group-item' element
 */
class TreeNode(private val root: SelenideElement) : BaseWidget(root) {
    private val expander = find("span[data-role=expander]")
    private val checkbox = find("span[data-role=checkbox]")
    private val realCheckbox = checkbox.find("input[type=checkbox]")
    private val textNode = find("span[data-role=display]")

    val dataId: String
        get() = dataId(this.root)

    val expanded
        get() = expander.getAttribute("data-mode") == "open"

    override fun getText(): String = textNode.text

    /**
     * Check if plus/minus exists
     */
    private val expandable
        get() = expander.find("i.gj-icon").exists()

    /**
     * Expand or wrap item if
     */
    fun expand(value: Boolean) {
        if ((expandable) && (expanded != value))
            expander.click()
    }

    /**
     * Select (or deselect) checkbox if exist and shown, otherwise do nothing
     */
    fun select(selected: Boolean) {
        if ((checkbox.isDisplayed) && (selected != realCheckbox.isSelected))
            checkbox.click(1, 1)  // workaround for not clicking upper lvl chechboxes
    }

}

class TreeView(root: SelenideElement) : BaseWidget(root) {
    constructor(cssSelector: String) : this(rootFind(cssSelector))

    /**
     * Returns path to node as nodes sorted by depth
     */
    private fun getNodeChain(dataId: String): List<TreeNode> {
        val path = dataId.split(".")
        var subpath = ""
        val treeNodeList = path.map {
            subpath = if (subpath != "") "$subpath.$it" else it
            val lvl = subpath.count { char -> char == '.' }
            return@map lvl to TreeNode(find("li[data-id='$subpath']"))
        }
        return treeNodeList
            .sortedBy { it.first }
            .map { it.second }
    }

    @Step
    fun selectByDataId(dataIDs: List<String>) {
        dataIDs
            .asSequence()
            .map { getNodeChain(it) }
            .forEach { list ->
                list.forEach {
                    // Select only desired ones
                    if (it.dataId in dataIDs) it.select(true) else it.expand(true)
                }
            }
    }
}