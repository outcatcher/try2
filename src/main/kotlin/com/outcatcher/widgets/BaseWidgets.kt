package com.outcatcher.widgets

import com.codeborne.selenide.Condition
import com.codeborne.selenide.ElementsCollection
import com.codeborne.selenide.Selenide
import com.codeborne.selenide.SelenideElement
import org.openqa.selenium.By

private val ROOT_ELEMENT = Selenide.`$`("html")

internal fun rootFind(cssSelector: String) = Selenide.`$`(cssSelector)

/**
 * Base class for com.outcatcher.widgets, delegating all methods to element used
 */
abstract class BaseWidget(private val rootElement: SelenideElement = ROOT_ELEMENT) : SelenideElement by rootElement {

    constructor(cssSelector: String) : this(rootFind(cssSelector))

    override fun find(cssSelector: String): SelenideElement {
        return rootElement.find(cssSelector)!!
    }

    override fun find(by: By): SelenideElement {
        return rootElement.find(by)!!
    }

    override fun findAll(by: By): ElementsCollection {
        return rootElement.findAll(by)!!
    }

    override fun findAll(cssSelector: String): ElementsCollection {
        return rootElement.findAll(cssSelector)!!
    }

    fun assure(condition: Condition): SelenideElement = this.should(condition)

}