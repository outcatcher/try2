@file:Suppress("UNCHECKED_CAST")

package com.outcatcher.elements

import com.codeborne.selenide.SelenideElement
import org.openqa.selenium.By
import com.codeborne.selenide.Selenide.`$` as baseFind

enum class Elements(val cls: Any) {
    Input(TextInput::class.java),
}

infix fun <T> SelenideElement.`as`(type: Class<T>): T {
    val newObject = type.getDeclaredConstructor(SelenideElement::class.java)!!.newInstance(this)
    return newObject as T
}

infix fun <T> SelenideElement.`as`(alias: Elements): T {
    val cls = alias.cls as Class<T>
    return this `as` cls
}

class TextInput(wrappedElement: SelenideElement) : BaseElement(wrappedElement) {
    constructor(cssSelector: String) : this(baseFind(cssSelector))
    constructor(by: By) : this(baseFind(by))

    fun input(text: String) {
        value = text
    }
}