package com.outcatcher.elements

import com.codeborne.selenide.SelenideElement

abstract class BaseElement(wrappedElement: SelenideElement) : SelenideElement by wrappedElement
