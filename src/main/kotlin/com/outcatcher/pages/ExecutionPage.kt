package com.outcatcher.pages

import io.qameta.allure.Step
import com.outcatcher.widgets.Message
import com.outcatcher.widgets.StartExecutionWidget


class ExecutionPage : BasePage("/execution") {

    val result = Message("#request-result")

    @Step("Start {type} execution on {environment}")
    fun startExecution(
        environment: String, type: String,
        email: String? = null, dryRun: Boolean = true,
        suiteIDs: List<String> = listOf()
    ) {
        with(StartExecutionWidget) {
            this.environment.selectOption(environment)
            this.typeOfTest.selectOption(type)
            this.suites.selectByDataId(suiteIDs)
            this.dryRun.isSelected = dryRun
            if (email != null) this.email.input(email)
            this.startButton.scrollTo().click()
        }
    }
}