package com.outcatcher.pages

import com.codeborne.selenide.Condition.visible
import com.outcatcher.widgets.LoginWidget
import io.qameta.allure.Step

class LoginPage : BasePage("/login") {

    private val loginWidget = LoginWidget()

    @Step("Login as {username} / {password}")
    fun signIn(username: String, password: String) {
        open()
        with(loginWidget) {
            assure(visible)
            this.username.input(username)
            this.password.input(password)
            signIn.click()
        }
    }
}
