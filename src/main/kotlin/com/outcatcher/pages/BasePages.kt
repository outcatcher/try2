package com.outcatcher.pages

import com.codeborne.selenide.Configuration
import com.codeborne.selenide.Selenide
import com.codeborne.selenide.WebDriverRunner
import io.qameta.allure.Allure.step
import io.qameta.allure.model.Status
import java.time.Duration
import java.util.concurrent.TimeoutException

abstract class BasePage(private val url: String) {

    private val fullUrl = if (url.startsWith("http")) url else Configuration.baseUrl + url

    private fun assureOpen() {
        val timeout = Duration.ofSeconds(5)
        val endTime = System.nanoTime() + timeout.nano
        val condition = { fullUrl == WebDriverRunner.url() }
        while (!condition()) {
            if (System.nanoTime() > endTime) {
                throw TimeoutException("Page $fullUrl is not open in ${timeout.seconds} seconds")
            }
            Thread.sleep(10)
        }
    }

    fun open() {
        step("Open `$url` Page")
        Selenide.open(url)
        assureOpen()
    }

    fun shouldBeOpen() {
        val actualUrl = WebDriverRunner.url()
        val ok = (fullUrl == actualUrl)
        val status = if (ok) Status.PASSED else Status.FAILED
        step("Page `$url` should be open", status)
        if (!ok) throw AssertionError("Page `$actualUrl` is open instead of $url")
    }
}